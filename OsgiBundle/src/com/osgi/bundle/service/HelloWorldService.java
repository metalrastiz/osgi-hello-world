package com.osgi.bundle.service;

import com.osgi.example.interfaces.HelloWorld;

public class HelloWorldService implements HelloWorld {
	
	@Override
	public void init() {
		System.out.println("Hello OSGi World!");
	}

}
