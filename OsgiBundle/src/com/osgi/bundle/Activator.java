package com.osgi.bundle;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

import com.osgi.bundle.service.HelloWorldService;
import com.osgi.example.interfaces.HelloWorld;

public class Activator implements BundleActivator {
	private ServiceRegistration service;
	
	@Override
	public void start(BundleContext context) throws Exception {
		HelloWorldService hw = new HelloWorldService();
		service = context.registerService(HelloWorld.class.getName(),hw, null);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		service.unregister();
	}

}
