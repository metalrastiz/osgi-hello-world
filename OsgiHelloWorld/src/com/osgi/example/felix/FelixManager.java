package com.osgi.example.felix;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.felix.framework.Felix;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;


public class FelixManager {
	private Felix felix;
	public static String BUNDLES_CACHE_PATH = "/cache";

    public FelixManager(String rootPath) {

    	Properties felixProperties = new FelixProperties(rootPath);
    	File cacheDir = new File(rootPath+BUNDLES_CACHE_PATH);
    	if (!cacheDir.exists()) {
    		if (!cacheDir.mkdirs()) {
    			throw new IllegalStateException("Unable to create felixcache dir");
    		}
    	}

    	try {
    		felix = new Felix(felixProperties);
    		felix.start();
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }

	public void installBundle(String bundle) {
		try {
			InputStream is = new FileInputStream(bundle);
			Bundle bb = felix.getBundleContext().installBundle(bundle,is);
			bb.start();
		} catch (BundleException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public <T> T getBundle(Class<T> clazz) {
		ServiceReference s = felix.getBundleContext().getServiceReference(clazz.getName());
		felix.getBundleContext().getService(s);
		return (T)felix.getBundleContext().getService(s);
	}
	
	public void startFelix() {
		try {
			felix.start();
		} catch (BundleException e) {
			e.printStackTrace();
		}
	}
	
	public void stopFelix() {
		
		try {
			felix.stop();
			felix.waitForStop(0);
		} catch (BundleException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}

