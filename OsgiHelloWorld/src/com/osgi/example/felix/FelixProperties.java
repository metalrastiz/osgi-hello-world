package com.osgi.example.felix;

import java.util.Properties;

public class FelixProperties extends Properties {
	private static final long serialVersionUID = 1L;
	
	public FelixProperties(String rootpath) {
		put("org.osgi.framework.storage",FelixManager.BUNDLES_CACHE_PATH);
		put("felix.cache.rootdir",rootpath);
		put("felix.log.level", "4");
		put("felix.startlevel.bundle", "1");
		put("org.osgi.framework.system.packages.extra", APP_PACKAGES_TO_EXPORT);
	}
	
	private final String APP_PACKAGES_TO_EXPORT = 
			"com.osgi.example.interfaces;";
	
	// If you are using felix on android, you must export android packages to see them in the bundles
	private final String ANDROID_PACKAGES_FOR_EXPORT= 
		// ANDROID 
        "android, " + 
        "android.app," + 
        "android.bluetooth,"+
        "android.bluetooth.BluetoothAdapter,"+
        "android.bluetooth.BluetoothDevice," +
        "android.content," +
        "android.content.res," +
        "android.database," + 
        "android.database.sqlite," + 
        "android.graphics, " + 
        "android.graphics.drawable, " + 
        "android.graphics.glutils, " + 
        "android.hardware, " + 
        "android.location, " + 
        "android.media, " + 
        "android.net, " + 
        "android.opengl, " + 
        "android.os, " +
        "android.os.Handler, " +
        "android.os.Looper, " + 
        "android.provider, " + 
        "android.preference, " +
        "android.sax, " + 
        "android.speech.recognition, " + 
        "android.telephony, " + 
        "android.telephony.gsm, " + 
        "android.text, " + 
        "android.text.method, " + 
        "android.text.style, " + 
        "android.text.util, " + 
        "android.util, " + 
        "android.view, " +
        "android.view.animation, " + 
        "android.webkit, " + 
        "android.widget, " + 
        "android.util, " +
        // JAVAx
        "javax.crypto; " + 
        "javax.crypto.interfaces; " + 
        "javax.crypto.spec; " + 
        "javax.microedition.khronos.opengles; " + 
        "javax.net; " + 
        "javax.net.ssl; " + 
        "javax.security.auth; " + 
        "javax.security.auth.callback; " + 
        "javax.security.auth.login; " + 
        "javax.security.auth.x500; " + 
        "javax.security.cert; " + 
        "javax.sound.midi; " + 
        "javax.sound.midi.spi; " + 
        "javax.sound.sampled; " + 
        "javax.sound.sampled.spi; " + 
        "javax.sql; " + 
        "javax.xml.parsers; " + 
        //JUNIT
        "junit.extensions; " + 
        "junit.framework; " + 
        //APACHE
        "org.apache.commons.codec; " + 
        "org.apache.commons.codec.binary; " + 
        "org.apache.commons.codec.language; " + 
        "org.apache.commons.codec.net; " + 
        "org.apache.commons.httpclient; " + 
        "org.apache.commons.httpclient.auth; " + 
        "org.apache.commons.httpclient.cookie; " + 
        "org.apache.commons.httpclient.entity; " + 
        "org.apache.commons.httpclient.methods; " + 
        "org.apache.commons.httpclient.methods.multipart; " + 
        "org.apache.commons.httpclient.params; " + 
        "org.apache.commons.httpclient.protocol; " + 
        "org.apache.commons.httpclient.util; " + 
		"org.apache.http;" +
		"org.apache.http.client;" +
		"org.apache.http.client.entity;" +
		"org.apache.http.client.methods;" +
		"org.apache.http.impl.client;" +
		"org.apache.http.message;" +
         
        //OTHERS
        "org.bluez; " + 
        "org.json; " + 
        "org.w3c.dom; " + 
        "org.xml.sax; " + 
        "org.xml.sax.ext; " + 
        "org.xml.sax.helpers; ";

        // APP PACKAGES
	
}

