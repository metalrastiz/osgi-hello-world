package com.osgi.example.app;

import com.osgi.example.felix.FelixManager;
import com.osgi.example.interfaces.HelloWorld;

public class App {
	private static String BUNDLE = "bundles/osgibundle.helloworld.jar";
	private FelixManager felixManager;
	
	public App(FelixManager felix) {
		felixManager = felix;
	}
	
	public void init() {
		felixManager.installBundle(BUNDLE);
		HelloWorld hwService = felixManager.getBundle(HelloWorld.class);
		hwService.init();
	}

}
