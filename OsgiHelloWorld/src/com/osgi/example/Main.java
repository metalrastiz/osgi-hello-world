package com.osgi.example;

import com.osgi.example.app.App;
import com.osgi.example.felix.FelixManager;

public class Main {
	
	public static void main(String... args) {
		FelixManager felix = new FelixManager("bundles/");
		App app = new App(felix);
		app.init();
		felix.stopFelix();
	}
}